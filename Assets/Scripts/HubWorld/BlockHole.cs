﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockHole : MonoBehaviour
{
    //public int monoSide = 1;
    //public int blockHole = 1;
    //public bool hasBlock = false;

    int blocks = 0;
    bool blockInserting = false;

    public float dist = .4f;
    public float waitTime = .01f;
    public float speed = 500f;

    // Start is called before the first frame update
    void Start()
    {
        blocks = GameObject.FindWithTag("GameStateController").GetComponent<GameState>().insertedBlocks;
        UpdateText();
        /*
        if (GameObject.FindWithTag("GameStateController").GetComponent<GameState>().GetBlockState(monoSide, blockHole))
        {
            hasBlock = true;
            transform.GetChild(0).gameObject.SetActive(true);
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (!hasBlock && GameObject.FindWithTag("GameStateController").GetComponent<GameState>().GetBlockState(monoSide, blockHole))
        {
            hasBlock = true;
            transform.GetChild(0).gameObject.SetActive(true);
        }
        */
    }

    void OnDestroy()
    {
        if (GameObject.FindWithTag("GameStateController") != null)
            GameObject.FindWithTag("GameStateController").GetComponent<GameState>().BlockInserted();
    }

    public void InsertBlock()
    {
        if (!blockInserting)
        {
            GameObject.FindWithTag("HUD").GetComponent<HUD>().DecreaseBlockNum();
            blocks++;
            StartCoroutine(InsertingBlock());
            blockInserting = true;
            GameObject.FindWithTag("GameStateController").GetComponent<GameState>().BlockInserting();
        }
    }

    IEnumerator InsertingBlock()
    {
        //Debug.Log("Opening Wall");
        Transform block = transform.GetChild(0).transform;
        Vector3 originalPos = block.position;

        block.gameObject.SetActive(true);

        /*
        for (int i = 0; i < dist * (speed)/2.5f; i++)
        {
            yield return new WaitForSeconds(waitTime);
        }
        */

        for (int i = 0; i < dist * (speed); i++)
        {
            //if (direction == Direction.Left)
            //    transform.Translate(Vector3.left / speed);
            //else
            transform.GetChild(0).transform.Translate(Vector3.forward / speed);

            //Debug.Log(i + " | " + (-transform.right) / speed);
            //Debug.Log(i + " | " + dist * speed);

            yield return new WaitForSeconds(waitTime);
        }

        //blocks++;
        UpdateText();
        /*
        hasBlock = true;
        transform.GetChild(0).gameObject.SetActive(true);

        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().NewBlock(monoSide, blockHole);
        */

        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().BlockInserted();

        block.gameObject.SetActive(false);
        block.position = originalPos;
        blockInserting = false;
    }

    void UpdateText()
    {
        transform.GetChild(1).GetComponent<TextMesh>().text = blocks.ToString();
    }
}
