﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMovement : MonoBehaviour, BlockControllerMsgTarget
{
    public bool holdKeyEnable = false;

    const short FORWARD = 0, RIGHT = 1, BACK = 2, LEFT = 3;

    GameObject cameraObj;

    bool moveLocked = false;
    bool win = false;
    bool shorted = false;

    // Start is called before the first frame update
    void Start()
    {
        cameraObj = Camera.main.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (holdKeyEnable)
        {
            if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(FORWARD);
            }

            else if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(BACK);
            }

            else if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(LEFT);
            }

            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(RIGHT);
            }
        }
        else 
        {
            if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(FORWARD);
            }

            else if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(BACK);
            }

            else if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(LEFT);
            }

            else if ((Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) && !moveLocked && !win)
            {
                moveLocked = true;
                UpdateDirection(RIGHT);
            }
        }
    }

    // updates the direction based off where the camera is
    void UpdateDirection(short dir)
    {
        Vector3 direction = Vector3.zero;

        GameObject camCopy = Object.Instantiate(cameraObj, cameraObj.transform);
        camCopy.SetActive(false);
        camCopy.transform.eulerAngles = new Vector3(0, cameraObj.transform.eulerAngles.y, 0);

        switch (dir)
        {
            case FORWARD:
                direction = Vector3.forward;
                break;
            case RIGHT:
                direction = Vector3.right;
                break;
            case BACK:
                direction = Vector3.back;
                break;
            case LEFT:
                direction = Vector3.left;
                break;
        }

        direction = camCopy.transform.TransformDirection(direction);

        Destroy(camCopy);

        if (direction.x < 0 && direction.z > 0)
            dir = FORWARD;
        else if (direction.x > 0 && direction.z > 0)
            dir = RIGHT;
        else if (direction.x > 0 && direction.z < 0)
            dir = BACK;
        else if (direction.x < 0 && direction.z < 0)
            dir = LEFT;

        MoveValidCheck(dir);
    }

    void MoveValidCheck(short dir)
    {
        // Will check to see if the next square is a valid square
        if (dir == FORWARD)
        {

            Vector3 test;
            test.x = transform.position.x;
            test.y = transform.position.y - 0.5f;
            test.z = transform.position.z + 1;

            Collider[] testLocations = Physics.OverlapSphere(test, 0.1f);

            if (testLocations.Length == 0)
            {
                moveLocked = false;
                // Do nothing, because you cannot go there
            }

            else
            {
                GetComponent<BlockController>().Move(dir);
            }
        }

        else if (dir == BACK)
        {
            Vector3 test;
            test.x = transform.position.x;
            test.y = transform.position.y - 0.5f;
            test.z = transform.position.z - 1;

            Collider[] testLocations = Physics.OverlapSphere(test, 0.1f);

            if (testLocations.Length == 0)
            {
                moveLocked = false;
                // Do nothing, because you cannot go there
            }

            else
            {
                GetComponent<BlockController>().Move(dir);
            }
        }

        else if (dir == LEFT)
        {
            Vector3 test;
            test.x = transform.position.x - 1;
            test.y = transform.position.y - 0.5f;
            test.z = transform.position.z;

            Collider[] testLocations = Physics.OverlapSphere(test, 0.1f);

            if (testLocations.Length == 0)
            {
                moveLocked = false;
                // Do nothing, because you cannot go there
            }

            else
            {
                GetComponent<BlockController>().Move(dir);
            }
        }

        else if (dir == RIGHT)
        {
            Vector3 test;
            test.x = transform.position.x + 1;
            test.y = transform.position.y - 0.5f;
            test.z = transform.position.z;

            Collider[] testLocations = Physics.OverlapSphere(test, 0.1f);

            if (testLocations.Length == 0)
            {
                moveLocked = false;
                // Do nothing, because you cannot go there
            }

            else
            {
                GetComponent<BlockController>().Move(dir);
            }
        }
    }

    public void LockMoving(bool locked) { moveLocked = locked; }

    public void AfterMove()
    {
        if (!shorted)
            moveLocked = false;
    }

    public void AfterUndo()
    {
        moveLocked = false;
        shorted = false;
    }

    public void AfterRestart()
    {
        moveLocked = false;
        shorted = false;
    }

    public void AfterWarp()
    {
        moveLocked = false;
        shorted = false;
    }

    public void Shorted()
    {
        moveLocked = true;
        shorted = true;
    }

    public void Win()
    {
        moveLocked = true;
        win = true;
    }

    public void DisableMove()
    {
        moveLocked = true;
    }

    public void EnableMove()
    {
        moveLocked = false;
    }
}
