﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameState : MonoBehaviour
{
    public bool hubTesting = false;
    public bool restartPlayerPositionOnQuit = false;

    public bool newGame = false;

    public bool[] levelsBeaten = new bool[27];
    public int[] levelScores = new int[27];

    public int score = 0;

    [Range(0, 4)]
    public int openedWalls = 0;
    [Range(0, 2)]
    public int openedWall2s = 0;
    public int wallTwoOpen = 5;
    public int wallThreeOpen = 12;
    public int wallFourOpen = 50;

    public bool openingWall = false;

    public int blocks = 0;
    public int insertedBlocks = 0;

    public bool win = false;
    public int currentLevel = 1;

    public static GameState instance;

    bool doneInserting = true;

    // for collecting the block after winning a level
    Vector3 levelSelectorPos = Vector3.zero;
    public void SetLevelSelectorPos(Vector3 pos) { levelSelectorPos = pos; }
    public Vector3 GetLevelSelectorPos() { return levelSelectorPos; }
    public bool collectBlock = false;

    bool updateScore = false;
    public void SetUpdateScore(bool update) { updateScore = update; }

    bool newWin = false;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            DestroyImmediate(this);
    }

    void OnApplicationQuit()
    {
        if (hubTesting && restartPlayerPositionOnQuit)
            StateSaveSystem.DeletePlayer();
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        
        if (hubTesting)
            LoadSave();
    }

    // Update is called once per frame
    void Update()
    {
        if (win && !levelsBeaten[currentLevel - 1])
        {
            collectBlock = true;
            levelsBeaten[currentLevel - 1] = true;
            newWin = true;
            win = false;
        }
        else if (win && levelsBeaten[currentLevel - 1])
            win = false;

        if (newWin && updateScore)
        {
            blocks += 1;
            newWin = false;
            StateSaveSystem.SaveProgress(this);
            updateScore = false;
            GameObject.FindWithTag("HUD").GetComponent<HUD>().UpdateText();
        }

        if (openedWalls == 0 && levelsBeaten[0] && !newWin && doneInserting)
        {
            openedWalls = 1;
            openingWall = true;
            StateSaveSystem.SaveProgress(this);
        }
        if (openedWalls == 1 && insertedBlocks == wallTwoOpen && doneInserting)
        {
            openedWalls = 2;
            openingWall = true;
            SaveState();
        }
        if (openedWalls == 2 && insertedBlocks == wallThreeOpen && doneInserting)
        {
            openedWalls = 3;
            openingWall = true;
            SaveState();
        }
        if (openedWalls == 3 && insertedBlocks == wallFourOpen && doneInserting)
        {
            openedWalls = 4;
            openingWall = true;
            SaveState();
        }
    }

    public bool GetBlock()
    {
        if (blocks > 0)
        {
            return true;
        }
        return false;
    }

    /*
    public bool GetBlockState(int monoSide, int blockHole)
    {
        switch (monoSide)
        {
            case 1:
                return mono1[blockHole - 1];
            case 2:
                return mono2[blockHole - 1];
            case 3:
                return mono3[blockHole - 1];
            case 4:
                return mono4[blockHole - 1];
        }
        return false;
    }
    */
    public void BlockInserting()
    {
        /*
        switch (monoSide)
        {
            case 1:
                mono1[blockHole-1] = true;
                break;
            case 2:
                mono2[blockHole-1] = true;
                break;
            case 3:
                mono3[blockHole-1] = true;
                break;
            case 4:
                mono4[blockHole-1] = true;
                break;
        }
        */

        doneInserting = false;
        insertedBlocks++;
        blocks--;
        SaveState();
    }

    public void BlockInserted()
    {
        doneInserting = true;
    }


    public void SaveState()
    {
        if (GameObject.FindWithTag("Player"))
        {
            GameObject.FindWithTag("Player").GetComponent<Player>().SaveState();
            StateSaveSystem.SaveProgress(this);
        }
    }

    public void LoadState()
    {
        GameObject.FindWithTag("Player").GetComponent<Player>().LoadState();
    }

    public void LoadSave()
    {
        if (newGame)
        {
            StateSaveSystem.DeletePlayer();
        }
        else
        {
            Debug.Log("loading save");

            ProgressData data = StateSaveSystem.LoadProgress();

            if (data != null)
            {

                levelsBeaten = new bool[data.levelsBeaten.Length];
                for (int i = 0; i < levelsBeaten.Length; i++)
                {
                    levelsBeaten[i] = data.levelsBeaten[i];
                }

                levelScores = new int[data.levelScores.Length];
                for (int i = 0; i < levelScores.Length; i++)
                {
                    levelScores[i] = data.levelScores[i];
                }

                openedWalls = data.openedWalls;
                openedWall2s = data.openedWall2s;

                blocks = data.blocks;
                insertedBlocks = data.insertedBlocks;

                score = data.score;
            }
        }
    }

    public void UpdateLevelScore(int newScore)
    {
        if (levelScores[currentLevel] < newScore)
        {
            levelScores[currentLevel] = newScore;
            CalcNewScore();
        }
    }

    public void CalcNewScore()
    {
        int newScore = 0;
        for (int i = 0; i < levelScores.Length; i++)
        { 
            newScore += levelScores[i];
        }
        score = newScore;

        SaveState();
    }
}
