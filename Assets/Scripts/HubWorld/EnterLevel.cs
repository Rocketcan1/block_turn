﻿// I DONT THINK THIS IS USED ANYWHERE BUT IM AFRAID TO DELETE IT
// GO TO LEVELSELECTCONTROLLER FOR THAT



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterLevel : MonoBehaviour
{
    public int level = 1;

    void Start()
    {
    }

    public void LoadScene()
    {
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().SaveState();
        
        StartCoroutine(LoadAsyncScene());
    }

    IEnumerator LoadAsyncScene()
    {
        // Fades out the screen
        Debug.Log("fading");
        GameObject.FindWithTag("HUD").GetComponent<HUD>().FadeToBlack();
        yield return new WaitForSeconds(GameObject.FindWithTag("HUD").GetComponent<HUD>().GetFadeDuration());
        Debug.Log("Done Fading");


        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(level);
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().currentLevel = level;

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
