﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProgressData
{
    public bool[] levelsBeaten;
    public int[] levelScores;
    public int openedWalls = 0;
    public int openedWall2s = 0;
    public int score = 0;

    public int blocks = 0;
    public int insertedBlocks = 0;

    public ProgressData(GameState gameState)
    {
        levelsBeaten = new bool[gameState.levelsBeaten.Length];
        for (int i = 0; i < levelsBeaten.Length; i++)
        {
            levelsBeaten[i] = gameState.levelsBeaten[i];
        }

        levelScores = new int[gameState.levelScores.Length];
        for (int i = 0; i < levelScores.Length; i++)
        {
            levelScores[i] = gameState.levelScores[i];
        }

        openedWalls = gameState.openedWalls;
        openedWalls = gameState.openedWall2s;

        blocks = gameState.blocks;
        insertedBlocks = gameState.insertedBlocks;

        score = gameState.score;
    }
}
