﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    Vector3 offset;
    Vector3 playerStartPosition;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
        playerStartPosition = player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (playerStartPosition.x != player.transform.position.x)
            offset.x = transform.position.x - player.transform.position.x;
        if (playerStartPosition.y != player.transform.position.y)
            offset.y = transform.position.y - player.transform.position.y;
        //offset.y += playerStartPosition.y - player.transform.position.y;

        transform.position = player.transform.position + offset;

        offset = transform.position - player.transform.position;


    }
}
