﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelScore : MonoBehaviour
{
    public BlockController scoreController;
    List<(short, short)> moveList;
    public Text scoreText;
    public int moveCount;

    void Start()
    {
        scoreController = GameObject.FindObjectOfType<BlockController>();
        moveList = scoreController.moveHist;
    }

    // Update is called once per frame
    void Update()
    {
        moveCount = moveList.Count;
        scoreText.text = "Move Count: " + moveCount.ToString();
    }
}
