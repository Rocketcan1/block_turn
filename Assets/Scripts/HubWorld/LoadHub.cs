﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadHub : MonoBehaviour
{
    bool fading = false;
    bool faded = true;
    public float fadeDuration = 1.0f;
    public Image fadeImage;

    float counter = 0.0f;

    void Start()
    {
        transform.GetChild(1).gameObject.SetActive(true);
        fadeImage.gameObject.SetActive(true);
        fadeImage.color = Color.black;
        //fadeImage.canvasRenderer.SetAlpha(1.0f);
        FadeFromBlack();
    }

    void Update()
    {
        if (fading && faded)
        {
            fadeImage.CrossFadeAlpha(0.0f, fadeDuration, false);
            fading = false;
            faded = false;
        }
        if (fading && !faded)
        {
            fadeImage.CrossFadeAlpha(1.0f, fadeDuration, false);
            fading = false;
            faded = true;
        }

        if (!faded && fadeImage.enabled && counter > fadeDuration)
        {
            //fadeImage.enabled = false;
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else if (!faded && fadeImage.enabled)
        {
            counter += Time.deltaTime;
        }
        else
        {
            counter = 0.0f;
        }
    }

    void FadeToBlack()
    {
        fadeImage.enabled = true;
        transform.GetChild(1).gameObject.SetActive(true);
        fadeImage.canvasRenderer.SetAlpha(0.0f);
        faded = false;
        fading = true;
    }

    void FadeFromBlack()
    {
        fadeImage.enabled = true;
        transform.GetChild(1).gameObject.SetActive(true);
        fadeImage.canvasRenderer.SetAlpha(1.0f);
        faded = true;
        fading = true;
    }

    public void ReturnToHubLevel()
    {
        //AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("HubWorld");
        StartCoroutine(LoadHubScene());
    }

    IEnumerator LoadHubScene()
    {
        // Fades out the screen
        FadeToBlack();
        yield return new WaitForSeconds(fadeDuration);

        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("HubWorld");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
