﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LoadState();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SaveState()
    {
        StateSaveSystem.SavePlayer(this);
    }

    public void LoadState()
    {
        GetComponent<CharacterController>().enabled = false;

        PlayerData data = StateSaveSystem.LoadPlayer();

        if (data != null)
        {
            //Debug.Log(data.position[2]);
            //Debug.Log(data.rotation[1]);

            this.transform.position = new Vector3(data.position[0], data.position[1], data.position[2]);

            //transform.GetChild(0).GetComponent<SmoothMouseLook>().rotationX = data.rotation[0];
            //transform.GetChild(0).GetComponent<SmoothMouseLook>().rotationY = data.rotation[1];

            GetComponent<PlayerController>().xRotation = data.rotation[0];
            transform.eulerAngles = new Vector3(0.0f, data.rotation[1], 0.0f);

            //Debug.Log(this.transform.position);
        }
        else
        {
            Debug.Log("no player data");
        }

        GetComponent<CharacterController>().enabled = true;
    }

    public void DisableMovement()
    {
        transform.GetComponent<PlayerController>().DisableMovement();
    }

    public void EnableMovement()
    {
        transform.GetComponent<PlayerController>().EnableMovement();
    }
}
