﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Image fadeImage;
    int blocks = 0;
    bool fading = false;
    bool faded = true;
    public float fadeDuration = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        blocks = GameObject.FindWithTag("GameStateController").GetComponent<GameState>().blocks;
        transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = blocks.ToString();

        fadeImage.color = Color.black;
        fadeImage.canvasRenderer.SetAlpha(1.0f);
        FadeFromBlack();
    }

    // Update is called once per frame
    void Update()
    {
        if (fading && faded)
        {
            fadeImage.CrossFadeAlpha(0.0f, fadeDuration, false);
            fading = false;
        }
        if (fading && !faded)
        {
            fadeImage.CrossFadeAlpha(1.0f, fadeDuration, false);
            fading = false;
        }
    }

    public void UpdateText()
    {
        blocks = GameObject.FindWithTag("GameStateController").GetComponent<GameState>().blocks;
        transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = blocks.ToString();
    }

    public void DecreaseBlockNum()
    {
        if (blocks > 0)
        {
            blocks--;
            transform.GetChild(1).GetComponent<UnityEngine.UI.Text>().text = blocks.ToString();
        }
    }

    public void FadeToBlack()
    {
        faded = false;
        fading = true;
    }

    public float GetFadeDuration() { return fadeDuration; }

    void FadeFromBlack()
    {
        faded = true;
        fading = true;
    }
}
