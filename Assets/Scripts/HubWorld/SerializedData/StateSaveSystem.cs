﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class StateSaveSystem
{
    public static string filePrefix = "/";

    public static void SavePlayer(Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + filePrefix + "player.sin";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(player);

        formatter.Serialize(stream, data);

        stream.Close();
    }

    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + filePrefix + "player.sin";

        Debug.Log(path);

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }

        //Debug.LogError("Save file not found in " + path);
        return null;
    }

    public static void DeletePlayer()
    {
        string path = Application.persistentDataPath + filePrefix + "player.sin";

        //Debug.Log("Deleting: " + path);

        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }

    public static ProgressData LoadProgress()
    {
        string path = Application.persistentDataPath + filePrefix + "progress.sin";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            ProgressData data = formatter.Deserialize(stream) as ProgressData;
            stream.Close();

            return data;
        }

        //Debug.LogError("Save file not found in " + path);
        return null;
    }

    public static void SaveProgress(GameState gameState)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + filePrefix + "progress.sin";
        FileStream stream = new FileStream(path, FileMode.Create);

        Debug.Log("saving progress at " + path);

        ProgressData data = new ProgressData(gameState);
        formatter.Serialize(stream, data);

        stream.Close();
    }
}
