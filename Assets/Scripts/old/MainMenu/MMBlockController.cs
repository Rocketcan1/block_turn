﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMBlockController : MonoBehaviour
{
    public Material lit;
    public float onIntensity = 0.25f;

    Color colorLit = new Color(1.0f, 1.0f, 0.5294f, 1.0f);
    float speed = 0.01F;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 6; i++)
        {
            this.transform.GetChild(i).GetComponent<MeshRenderer>().material = lit;
            //this.transform.GetChild(i).GetComponent<Light>().color = colorLit;
            //this.transform.GetChild(i).GetComponent<Light>().intensity = onIntensity;
        }

        StartCoroutine("rotate");
    }

    IEnumerator rotate()
    {
        while (1 == 1)
        {
            //this.transform.Translate(Vector3.up / 300, Space.World);
            this.transform.Rotate(0.0f, -0.25f, 0.0f, Space.Self);
            yield return new WaitForSeconds(speed);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
