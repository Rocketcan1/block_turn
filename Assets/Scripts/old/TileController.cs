﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileController : MonoBehaviour
{
    public GameObject emblem;
    public GameObject pointLight;
    public Material unlit;
    public Material lit;

    public bool isLightTile;
    public bool isPermanentlyLit;
    public bool isWarpTile;
    public bool isDropTile;

    //public float onIntensity = 0.25f;

    // used for lit tile
    bool isLit;
    bool firstState;


    // used for drop tiles (holds initialpos
    Vector3 initTilePos;


    /*
     * Intializers for each type of tile
     */
    // Start is called before the first frame update
    void Start()
    {
        if (isLightTile)
        {
            InitLightTile();
        }
        if (isWarpTile)
        {

        }
        if (isDropTile) 
        {
            InitDropTile();
        }
    }

    void InitLightTile()
    {
        if (emblem.GetComponent<MeshRenderer>().material.name == unlit.name + " (Instance)")
        {
            isLit = false;
            //emblem.GetComponent<Light>().intensity = 0.0f;
        }
        else
        {
            isLit = true;
            //emblem.GetComponent<Light>().intensity = onIntensity;
        }
        firstState = isLit;
    }

    void InitDropTile()
    {
        initTilePos = transform.position;
    }
    // END intializers

    /*
     * Controls what happens if tile is landed on
     */
    // Changes the tiles material from lit to blank or vise versa
    public void UpdateTile()
    {
        if(isLightTile)
            UpdateLightTile();
        if (isDropTile)
            UpdateDropTile();
    }

    void UpdateLightTile()
    {
        if (!isPermanentlyLit)
        {
            if (isLit)
            {
                emblem.GetComponent<MeshRenderer>().material = unlit;
                isLit = false;
                //emblem.GetComponent<Light>().intensity = 0.0f;
            }
            else
            {
                emblem.GetComponent<MeshRenderer>().material = lit;
                isLit = true;
                //emblem.GetComponent<Light>().intensity = onIntensity;
            }
        }
    }

    void UpdateDropTile()
    {
        if (transform.position != initTilePos)
            transform.position += Vector3.down * 4;
        else
        {
            transform.position = initTilePos;
        }
    }
    // END tile updater

    /*
     * Controls what happens when a restart occurs
     */
    public void RestartTile()
    {
        if (isLightTile)
            RestartLightTile();
        //if (isDropTile)
            //RestartDropTile();
    }

    void RestartLightTile()
    {
        isLit = firstState;

        if (isLit)
        {
            emblem.GetComponent<MeshRenderer>().material = lit;
            isLit = true;
            //emblem.GetComponent<Light>().intensity = onIntensity;
        }
        else
        {
            emblem.GetComponent<MeshRenderer>().material = unlit;
            isLit = false;
            //emblem.GetComponent<Light>().intensity = 0.0f;
        }
    }
    // END Restart

    // Returns true if the tile is lit, false if not
    public bool GetState()
    {
        return isLit;
    }
}
