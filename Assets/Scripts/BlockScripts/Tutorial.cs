﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour, BlockControllerMsgTarget
{
    public bool shortingTutorial = false;
    public bool restartingTutorial = false;
    public bool interfaceTutorial = false;

    public GameObject tile1;
    public GameObject tile2;

    bool movingTiles = false;
    bool moveEnabled = false;

    // shorting
    bool shorted = false;
    Vector3 direction = Vector3.right;

    // interface
    bool mustExamine = false;
    bool examined = false;

    public GameObject tutorialText1;
    public GameObject tutorialText2;

    public float dist = 1f;
    public float waitTime = .01f;
    public float speed = 100f;

    // Start is called before the first frame update
    void Start()
    {
        if (tutorialText1)
            tutorialText1.SetActive(false);
        if (tutorialText2)
            tutorialText2.SetActive(false);

        if (!interfaceTutorial)
        {
            GetComponent<BlockController>().disableUndo = true;
            GetComponent<BlockController>().disableRestart = true;
        }
        GetComponent<BlockController>().disableExamine = true;

        if (restartingTutorial)
            direction = Vector3.right;
    }

    // Update is called once per frame
    void Update()
    {
        if (!movingTiles && moveEnabled)
        {
            if (shortingTutorial)
                GetComponent<BlockController>().DisableMoving(true);
            StartCoroutine(MoveTiles());
            movingTiles = true;
        }
        else if (interfaceTutorial && !examined && mustExamine && Input.GetKey(KeyCode.Space))
        {
            //GetComponent<BlockController>().DisableMoving(false);
            GetComponent<KeyboardMovement>().LockMoving(false);
            //tutorialText1.SetActive(false);
            GetComponent<BlockController>().disableUndo = false;
            GetComponent<BlockController>().disableRestart = false;
            mustExamine = false;
            examined = true;
        }
    }

    IEnumerator MoveTiles()
    {
        Debug.Log("moving tiles");
        for (int i = 0; i < dist * (speed); i++)
        {
            if (tile1)
            {
                Debug.Log("tile1");
                tile1.transform.Translate(direction / speed, Space.World);
            }
            if (tile2)
            {
                tile2.transform.Translate(direction / speed, Space.World);
            }
            //if (direction == Direction.Left)
            //else
            //transform.Translate(Vector3.right / speed);

            //Debug.Log(i + " | " + (-transform.right) / speed);
            //Debug.Log(i + " | " + dist * speed);

            yield return new WaitForSeconds(waitTime);
        }

        Debug.Log("Done moving tiles");

        if (shortingTutorial)
        {
            tutorialText1.SetActive(true);
            GetComponent<BlockController>().disableUndo = false;
        }
        else if (restartingTutorial)
        {
            GetComponent<BlockController>().disableRestart = false;
            tutorialText1.SetActive(true);
        }

        if (!shortingTutorial)
        {
            GetComponent<KeyboardMovement>().LockMoving(false);
        }

        GetComponent<BlockController>().DisableMoving(false);
    }

    /*
     * BlockControllerMsgTarget functions (AfterMove, AfterUndo, AfterRestart)
     */
    public void AfterMove()
    {
        if (restartingTutorial && transform.position.x > 1.5f)
        {
            if (!shorted)
                GetComponent<BlockController>().disableUndo = true;
            moveEnabled = true;
        }
        else if (!examined && interfaceTutorial && transform.position.x > 2.5f && transform.position.z > 0.5f)
        {
            GetComponent<BlockController>().disableExamine = false;
            GetComponent<BlockController>().forceExamine = true;
            tutorialText1.SetActive(true);
            tutorialText2.SetActive(true);
            GetComponent<BlockController>().DisableMoving(true);
            GetComponent<KeyboardMovement>().LockMoving(true);
            GetComponent<BlockController>().disableUndo = true;
            GetComponent<BlockController>().disableRestart = true;
            mustExamine = true;
        }
    }

    public void AfterUndo() 
    {
        if (restartingTutorial)
        {
            shorted = false;
            GetComponent<BlockController>().disableUndo = true;
            //Debug.Log(transform.position);
        }
    }

    public void AfterRestart()
    {
        if (restartingTutorial)
        {
            restartingTutorial = false;
            GetComponent<BlockController>().disableUndo = false;
        }
    }

    public void AfterWarp() { }

    public void Shorted() 
    {
        if (shortingTutorial && !moveEnabled)
        {
            Debug.Log("shorted");
            shorted = true;
            //GetComponent<BlockController>().disableUndo = false;
            moveEnabled = true;
        }
        else if (restartingTutorial)
        {
            GetComponent<BlockController>().disableUndo = false;
            shorted = true;
        }
    }

    public void Win()
    {
        
    }

    public void DisableMove()
    {
    }

    public void EnableMove()
    {
    }
    /* */
}
