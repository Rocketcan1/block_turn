﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour, BlockControllerMsgTarget
{
    const short FORWARD = 0, RIGHT = 1, BACK = 2, LEFT = 3;

    List<(GameObject, short)> tiles = new List<(GameObject, short)>();

    bool moveLocked = false;
    bool win = false;

    // Start is called before the first frame update
    void Start()
    {
        UpdateTileList();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.K))
            UpdateTileList();
        
        if (Input.GetKeyDown(KeyCode.L))
            HighlightTiles();
        */

        // figures out which tile the mouse is over and issues a command to move to that tile
        if (!win && !moveLocked && Input.GetMouseButtonDown(0))
        {
            RaycastHit[] hits;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hits = Physics.RaycastAll(ray, 100.0f);

            for (int i = 0; i < hits.Length; i++)
            {
                Debug.Log(hits[i].transform.tag);
                if (hits[i].transform.tag == "Tile" || hits[i].transform.tag == "LightTile" || hits[i].transform.tag == "WarpTile")
                {
                    for (int j = 0; j < tiles.Count; j++)
                    {
                        if (hits[i].transform.gameObject == tiles[j].Item1)
                        {
                            GetComponent<BlockController>().Move(tiles[j].Item2);
                        }
                    }
                }
            }
        }

        /*
        else if (!win && Input.GetKeyDown(KeyCode.Z))
        {
            GetComponent<BlockStateAndMovement>().Undo();
        }
        else if (!win && Input.GetKeyDown(KeyCode.R))
        {
            GetComponent<BlockStateAndMovement>().Restart();
        }
        */
    }

    // Shoots a ray down originating from one unit forward, right, down, and left to get a list of tiles that can be moved to
    // stores these tiles in variable tiles
    void UpdateTileList()
    {
        tiles.Clear();

        List<RaycastHit> hits = new List<RaycastHit>();
        RaycastHit[] fHits, rHits, bHits, lHits;

        fHits = Physics.RaycastAll(transform.position + Vector3.forward, Vector3.down, 2);
        rHits = Physics.RaycastAll(transform.position + Vector3.right, Vector3.down, 2);
        bHits = Physics.RaycastAll(transform.position + Vector3.back, Vector3.down, 2);
        lHits = Physics.RaycastAll(transform.position + Vector3.left, Vector3.down, 2);

        foreach (RaycastHit hit in fHits)
        {
            if (hit.transform.tag == "Tile" || hit.transform.tag == "LightTile" || hit.transform.tag == "WarpTile")
                tiles.Add((hit.transform.gameObject, FORWARD));
        }
        foreach (RaycastHit hit in rHits)
        {
            if (hit.transform.tag == "Tile" || hit.transform.tag == "LightTile" || hit.transform.tag == "WarpTile")
                tiles.Add((hit.transform.gameObject, RIGHT));
        }
        foreach (RaycastHit hit in bHits)
        {
            if (hit.transform.tag == "Tile" || hit.transform.tag == "LightTile" || hit.transform.tag == "WarpTile")
                tiles.Add((hit.transform.gameObject, BACK));
        }
        foreach (RaycastHit hit in lHits)
        {
            if (hit.transform.tag == "Tile" || hit.transform.tag == "LightTile" || hit.transform.tag == "WarpTile")
                tiles.Add((hit.transform.gameObject, LEFT));
        }
    }

    void HighlightTiles()
    { 
    
    }

    public void AfterMove()
    {
        UpdateTileList();
    }

    public void AfterUndo()
    {
        moveLocked = false;
        UpdateTileList();
    }

    public void AfterRestart()
    {
        moveLocked = false;
        UpdateTileList();
    }

    public void AfterWarp()
    {
        UpdateTileList();
    }

    public void Shorted()
    {
        moveLocked = true;
    }

    public void Win()
    {
        moveLocked = true;
        win = true;
    }

    public void DisableMove()
    {
        moveLocked = true;
    }

    public void EnableMove()
    {
        moveLocked = false;
    }
}
