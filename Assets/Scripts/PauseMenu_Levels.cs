﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu_Levels : MonoBehaviour
{
    bool menuOpen = false;
    bool menuDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        transform.GetChild(0).gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!menuOpen && Input.GetKeyDown(KeyCode.Escape) && !menuDisabled)
        {
            OpenMenu();
        }
        else if (menuOpen && Input.GetKeyDown(KeyCode.Escape) && !menuDisabled)
        {
            CloseMenu();
        }
    }

    public void DisableMenu()
    {
        CloseMenu();
        menuDisabled = true;
    }

    public void OpenMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        GameObject.FindWithTag("Block").GetComponent<BlockController>().DisableMovement();
        transform.GetChild(0).gameObject.SetActive(true);
        menuOpen = true;
    }

    public void CloseMenu()
    {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        GameObject.FindWithTag("Block").GetComponent<BlockController>().EnableMovement();
        transform.GetChild(0).gameObject.SetActive(false);
        menuOpen = false;
    }

    public void ReturnToHub()
    {
        transform.GetComponent<LoadHub>().ReturnToHubLevel();
    }
}
