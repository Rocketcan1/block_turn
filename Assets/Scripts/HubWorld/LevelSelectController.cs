﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectController : MonoBehaviour
{
    public int level = 1;
    public bool won = false;

    bool lightOn = true;

    float intensity = 0.0f;

    void Start()
    {
        if (GameObject.FindWithTag("GameStateController").GetComponent<GameState>().levelsBeaten[level - 1])
            won = true;

        intensity = transform.GetChild(1).GetComponent<Light>().intensity;

        if (won)
        {
            transform.GetChild(1).GetComponent<Light>().intensity = 0.0f;
            lightOn = false;
        }
    }

    void Update()
    {
        if (won && lightOn)
        {
            transform.GetChild(1).GetComponent<Light>().intensity = 0.0f;
            lightOn = false;
        }
    }

    public void LoadScene()
    {
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().SaveState();
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().openingWall = false;
        StartCoroutine(LoadAsyncScene());
    }

    IEnumerator LoadAsyncScene()
    {
        //Drops the block in the hole
        while (GameObject.FindWithTag("Player").GetComponent<PlayerController>().GetBlockDropping())
        {
            yield return null;
        }
        //GameObject.FindWithTag("Player").GetComponent<PlayerController>().DropBlock();
        //ield return new WaitForSeconds(5);


        // Fades out the screen
        GameObject.FindWithTag("HUD").GetComponent<HUD>().FadeToBlack();
        yield return new WaitForSeconds(GameObject.FindWithTag("HUD").GetComponent<HUD>().GetFadeDuration());

        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(level);
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().currentLevel = level;

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
