﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpData : MonoBehaviour
{
    public int pair;

    public int getPair()
    {
        return pair;
    }

    public void setPair(int newPair)
    {
        pair = newPair;
    }
}
