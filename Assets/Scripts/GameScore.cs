﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScore : MonoBehaviour
{
    public int gameScore;
    public Text gameScoreText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        gameScore = GameObject.FindWithTag("GameStateController").GetComponent<GameState>().score;
        gameScoreText.text = "Score: " + gameScore.ToString();
    }
}
