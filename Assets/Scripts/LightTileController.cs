﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTileController : MonoBehaviour
{
    public Material unlit;
    public Material lit;
    public Material shorted;

    public float pointIntensity = .3f;

    public bool isPermanentlyLit;
    public bool newLight = false;
    //public float onIntensity = 0.25f;

    // used for lit tile
    bool isLit;
    bool firstState;

    // materials out of order for permanently lit tiles, this fixes that
    int meat = 0;

    // used for drop tiles (holds initialpos
    Vector3 initTilePos;

    /*
     * Intializers for each type of tile
     */
    // Start is called before the first frame update
    void Start()
    {
        pointIntensity = transform.GetChild(0).GetComponent<Light>().intensity;
        InitLightTile();
    }

    void InitLightTile()
    {
        if (isPermanentlyLit)
            meat = 0;

        if (!newLight)
        {
            if (GetComponent<MeshRenderer>().materials[meat].name == unlit.name + " (Instance)")
            {
                isLit = false;

                transform.GetChild(0).GetComponent<Light>().intensity = 0;
            }
            else
            {
                isLit = true;

                transform.GetChild(0).GetComponent<Light>().intensity = pointIntensity;
            }
        }
        else
        {
            if (transform.GetChild(1).GetComponent<MeshRenderer>().materials[meat].name == unlit.name + " (Instance)")
            {
                isLit = false;

                transform.GetChild(0).GetComponent<Light>().intensity = 0;
            }
            else
            {
                isLit = true;

                transform.GetChild(0).GetComponent<Light>().intensity = pointIntensity;
            }
        }
        firstState = isLit;
    }
    // END intializers

    /*
     * Controls what happens if tile is landed on
     */
    // Changes the tiles material from lit to blank or vise versa
    public void UpdateTile()
    {
        UpdateLightTile();
    }

    void UpdateLightTile()
    {
        if (!isPermanentlyLit)
        {
            if (isLit)
            {
                if (!newLight)
                {
                    Material[] mats = GetComponent<MeshRenderer>().materials;
                    mats[meat] = unlit;

                    GetComponent<MeshRenderer>().materials = mats;
                }
                else
                {
                    transform.GetChild(1).GetComponent<MeshRenderer>().material = unlit;
                }

                transform.GetChild(0).GetComponent<Light>().intensity = 0;

                isLit = false;
            }
            else
            {
                if (!newLight)
                {
                    Material[] mats = GetComponent<MeshRenderer>().materials;
                    mats[meat] = lit;

                    GetComponent<MeshRenderer>().materials = mats;
                }
                else
                {
                    transform.GetChild(1).GetComponent<MeshRenderer>().material = lit;
                }

                transform.GetChild(0).GetComponent<Light>().intensity = pointIntensity;

                isLit = true;
            }
        }
    }
    // END tile updater

    /*
     * Controls what happens when a restart occurs
     */
    public void RestartTile()
    {
        RestartLightTile();
    }

    void RestartLightTile()
    {
        isLit = firstState;

        if (isLit)
        {
            if (!newLight)
            {
                Material[] mats = GetComponent<MeshRenderer>().materials;
                mats[meat] = lit;

                GetComponent<MeshRenderer>().materials = mats;
            }
            else
            {
                transform.GetChild(1).GetComponent<MeshRenderer>().material = lit;
            }

            transform.GetChild(0).GetComponent<Light>().intensity = pointIntensity;

            isLit = true;
        }
        else
        {
            if (!newLight)
            {
                Material[] mats = GetComponent<MeshRenderer>().materials;
                mats[meat] = unlit;

                GetComponent<MeshRenderer>().materials = mats;
            }
            else
            {
                transform.GetChild(1).GetComponent<MeshRenderer>().material = unlit;
            }

            transform.GetChild(0).GetComponent<Light>().intensity = 0;

            isLit = false;
        }
    }
    // END Restart

    public void ShortTile()
    {
        transform.GetChild(1).GetComponent<MeshRenderer>().material = shorted;
        transform.GetChild(0).GetComponent<Light>().intensity = 0;
    }

    public void UnshortTile()
    {
        if (isLit)
        {
            transform.GetChild(1).GetComponent<MeshRenderer>().material = lit;
            transform.GetChild(0).GetComponent<Light>().intensity = pointIntensity;
        }
        else
        {
            transform.GetChild(1).GetComponent<MeshRenderer>().material = unlit;
            transform.GetChild(0).GetComponent<Light>().intensity = 0;
        }
    }

    // Returns true if the tile is lit, false if not
    public bool GetState()
    {
        return isLit;
    }
}
