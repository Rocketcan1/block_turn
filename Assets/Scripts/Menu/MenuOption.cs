﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuOption : MonoBehaviour
{
    string level;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadLevel(string levelName)
    {
        level = levelName;
        StartCoroutine(LoadAsyncScene());
    }

    IEnumerator LoadAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(level);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void SetNewGame(bool newGame)
    {
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().newGame = newGame;
    }

    public void SetFile(int save)
    {
        string filePrefix = "/1_";

        switch (save)
        {
            case 1:
                filePrefix = "/1_";
                break;
            case 2:
                filePrefix = "/2_";
                break;
            case 3:
                filePrefix = "/3_";
                break;
        }

        StateSaveSystem.filePrefix = filePrefix;
    }

    public void LoadHub()
    {
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().LoadSave();

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("HubWorld");
        StartCoroutine(LoadHubScene());
    }

    IEnumerator LoadHubScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("HubWorld");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
