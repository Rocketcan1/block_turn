﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface BlockControllerMsgTarget : IEventSystemHandler
{
    void AfterMove();

    void AfterUndo();

    void AfterRestart();

    void AfterWarp();

    void Shorted();

    void Win();

    void DisableMove();

    void EnableMove();
}

public class BlockController : MonoBehaviour, BlockControllerMsgTarget
{
    // playing state is defined as following (0 = no changes, 1 = tile effect caused, 2 = win (implies 1), -1 = loss)

    public int step = 9;
    public float speed = 0.01f;

    public float lightIntensity = 0.2f;

    public AudioClip moveSound;
    public AudioClip winSound;
    public AudioClip warpSound;

    public Text scoreText;

    // constants for movement directions where forward = +z right = +x
    const short FORWARD = 0, RIGHT = 1, BACK = 2, LEFT = 3;

    // Constants for each sides label
    const short TOP_SIDE = 0, FRONT_SIDE = 1, RIGHT_SIDE = 2, BACK_SIDE = 3, LEFT_SIDE = 4, BOTTOM_SIDE = 5;

    // needed for moving
    bool moving = false;
    Vector3 rotPoint, rotAxis;
    short moveDir;


    // holds the blockstatehud
    GameObject blockStateHUD;

    // needed for undoing
    public List<(short, short)> moveHist = new List<(short, short)>();   // (direction, playing state)
    bool undoing = false; // alerts movement that this is an undo

    // for restarting
    Vector3 originPos;
    Quaternion originRot;
    GameObject[] lightTiles;

    Camera mainCam;

    /*
     * used for controlling game state
     */
    // Materials for block
    public Material unlit;
    public Material lit;
    public Material shorted;
    short bottomSide = BOTTOM_SIDE;     // used for to get info about orientation
    bool[] sideState = new bool[] { false, false, false, false, false, false };   // Holds state of all 6 sides (false = unlit, true = lit)
    bool isShorted = false;
    public bool levelWon = false;

    public bool disableUndo = false;
    public bool disableRestart = false;
    public bool disableExamine = false;
    public bool forceExamine = false;
    //public bool disableAll = false;

    bool droppingIn = false;
    public float dropDistance = 10.0f;
    public float delay = 1.0f;
    float dropVelocity = 10.0f;
    float dropAcceleration = 9.81f;
    float rotationAngle = 270.0f;
    float fallTime = 0.0f;
    Quaternion newAngle;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;

        blockStateHUD = GameObject.FindWithTag("BlockStateHUD");
        originPos = this.transform.position;
        originRot = this.transform.rotation;
        mainCam = Camera.main;

        lightTiles = GameObject.FindGameObjectsWithTag("LightTile");

        blockStateHUD.GetComponent<BlockUIController>().UpdateHUD();
        droppingIn = true;
        fallTime = Mathf.Sqrt(2 * (dropDistance - originPos.y) / dropAcceleration);
    }

    // Update is called once per frame
    void Update()
    {
        // Drops the block in from up in the air
        if (droppingIn)
        {
            if (!moving)
            {
                transform.position = originPos + dropDistance * Vector3.up;
                transform.Rotate(rotationAngle, 0.0f, 0.0f, Space.World);
                transform.Rotate(0.0f, rotationAngle , 0.0f, Space.World);
                transform.Rotate(0.0f, 0.0f, rotationAngle, Space.World);
                //transform.Rotate(rotationAngle, rotationAngle, rotationAngle);

                newAngle = transform.rotation;
            }

            moving = true;

            if (transform.position.y > originPos.y)
            {
                if (delay < 0)
                {
                    dropVelocity += dropAcceleration * Time.deltaTime;
                    transform.position = transform.position - dropVelocity * Time.deltaTime * Vector3.up;

                    transform.rotation = Quaternion.Slerp(originRot, newAngle, (transform.position.y - originPos.y)/dropDistance);
                }
                else 
                {
                    delay -= Time.deltaTime;
                }
            }
            else 
            {
                transform.position = originPos;
                transform.rotation = originRot;
                moving = false;
                droppingIn = false;
            }

            //StartCoroutine(DropIn());
        }

        // undoes move
        if (!disableUndo && Input.GetKeyDown(KeyCode.Z) && !levelWon)
        {
            Undo();
        }
        // restarts the level
        else if (!disableRestart && Input.GetKeyDown(KeyCode.R) && !levelWon)
        {
            Restart();
        }
        // examines bottom of the block
        else if (forceExamine || (!disableExamine && Input.GetKeyDown(KeyCode.Space) && !levelWon && !moving && !isShorted))
        {
            forceExamine = false;
            StartCoroutine("Examine");
        }
    }

    public bool GetShorted() { return isShorted; }

    public void DisableMoving(bool isMoving) { moving = isMoving; }

    /*
     * Movement functions (move, undo, restart, float, examine)
     */
    public void Move(short dir) 
    {
        // Will check to see if the next square is a valid square
        if (!moving && dir == FORWARD)
        {
            rotPoint = transform.position + Vector3.forward * 0.5f + Vector3.down * 0.5f;
            rotAxis = Vector3.right;
            moveDir = dir;

            StartCoroutine("PerformMove");
            moving = true;
        }
        else if (!moving && dir == RIGHT)
        {
            rotPoint = transform.position + Vector3.right * 0.5f + Vector3.down * 0.5f;
            rotAxis = Vector3.back;
            moveDir = dir;

            StartCoroutine("PerformMove");
            moving = true;
        }
        else if (!moving && dir == BACK)
        {
            rotPoint = transform.position + Vector3.back * 0.5f + Vector3.down * 0.5f;
            rotAxis = Vector3.left;
            moveDir = dir;

            StartCoroutine("PerformMove");
            moving = true;
        }
        else if (!moving && dir == LEFT)
        {
            rotPoint = transform.position + Vector3.left * 0.5f + Vector3.down * 0.5f;
            rotAxis = Vector3.forward;
            moveDir = dir;

            StartCoroutine("PerformMove");
            moving = true;
        }
    }

    void Undo()
    {
        if (!moving && moveHist.Count > 0)
        {
            undoing = true;

            short dir = moveHist[moveHist.Count - 1].Item1;
            short playingState = moveHist[moveHist.Count - 1].Item2;

            moveHist.RemoveAt(moveHist.Count - 1);

            UndoState(playingState);

            switch (dir)
            {
                case FORWARD:
                    Move(BACK);
                    break;
                case RIGHT:
                    Move(LEFT);
                    break;
                case BACK:
                    Move(FORWARD);
                    break;
                case LEFT:
                    Move(RIGHT);
                    break;
            }
        }
    }

    void Restart()
    {
        if (!moving)
        {
            moveHist.Clear();

            transform.position = originPos;
            transform.rotation = originRot;

            RestartState();
        }
    }

    IEnumerator PerformMove()
    {
        AudioSource.PlayClipAtPoint(moveSound, transform.position);

        for (int i = 0; i < (90 / step); i++)
        {
            transform.RotateAround(rotPoint, rotAxis, step);
            yield return new WaitForSeconds(speed);
        }

        ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.AfterMove());
    }

    public float floatSpeed = 200;
    public float floatTime = 1.0f;

    IEnumerator Float()
    {
        float initTime = Time.time;
        bool endingLevel = false;
        while (true)
        { 
            transform.Translate(Vector3.up / floatSpeed, Space.World);
            transform.Rotate(0.0f, -0.25f, 0.0f, Space.World);

            if (!endingLevel && (Time.time - initTime) > floatTime)
            {
                endingLevel = true;
                GameObject.FindWithTag("PauseMenu").GetComponent<LoadHub>().ReturnToHubLevel();
            }
            yield return new WaitForSeconds(speed);
        }
    }

    IEnumerator Examine()
    {
        moving = true;

        int steps = 15;
        float interval = 90.0f / (float)steps;
        float height = 0.7f;
        float floatingRange = 0.001f;
        float tiltAngle = 70.0f;
        float increment = (tiltAngle / steps);

        Vector3 rotAxis;
        { // figures out rotation axis depending on the cameras angle
            Vector3 camPos = mainCam.gameObject.transform.position;
            camPos.y = transform.position.y;

            Vector3 localRotAxis = Vector3.Normalize(camPos - transform.position);

            if (localRotAxis.x > 0 && localRotAxis.z > 0)
                rotAxis = Vector3.left + Vector3.forward;
            else if (localRotAxis.x < 0 && localRotAxis.z > 0)
                rotAxis = Vector3.left + Vector3.back;
            else if (localRotAxis.x < 0 && localRotAxis.z < 0)
                rotAxis = Vector3.right + Vector3.back;
            else
                rotAxis = Vector3.right + Vector3.forward;
        }


        Vector3 ogPos = transform.position;
        Vector3 exmPos = ogPos + new Vector3(0, height, 0);
        Quaternion ogRot = transform.rotation;

        for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
        {
            transform.position = new Vector3(
                transform.position.x,
                Mathf.Lerp(ogPos.y, exmPos.y + floatingRange, Mathf.Sin(i * interval * Mathf.Deg2Rad)),
                transform.position.z);

            //transform.RotateAround(transform.position, rotAxis, increment);

            yield return new WaitForSeconds(speed);
        }

        //Quaternion rotateAngle;

        while (Input.GetKey(KeyCode.Space))
        {
            if (Input.GetKey(KeyCode.W))
            {
                for (int i = 0; Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Space); i++)
                {
                    if (i < steps)
                        transform.RotateAround(transform.position, /*rotAxis*/ Vector3.right, increment * 1.25f);
                    yield return new WaitForSeconds(speed);
                }
                for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
                {
                    transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));
                    yield return new WaitForSeconds(speed);
                }
            }
            else if (Input.GetKey(KeyCode.S))
            {
                for (int i = 0; Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Space); i++)
                {
                    if (i < steps)
                        transform.RotateAround(transform.position, /*rotAxis*/ Vector3.left, increment);
                    yield return new WaitForSeconds(speed);
                }
                for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
                {
                    transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));
                    yield return new WaitForSeconds(speed);
                }
            }
            else if (Input.GetKey(KeyCode.D))
            {
                for (int i = 0; Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.Space); i++)
                {
                    if (i < steps)
                        transform.RotateAround(transform.position, Vector3.up, -increment * 1.1f);
                    yield return new WaitForSeconds(speed);
                }
                for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
                {
                    transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));
                    yield return new WaitForSeconds(speed);
                }
            }
            else if (Input.GetKey(KeyCode.A))
            {
                for (int i = 0; Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.Space); i++)
                {
                    if (i < steps)
                        transform.RotateAround(transform.position, Vector3.up, increment * 1.5f);
                    yield return new WaitForSeconds(speed);
                }
                for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
                {
                    transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));
                    yield return new WaitForSeconds(speed);
                }
            }

            yield return new WaitForSeconds(speed);
        }
        /*
        for (int i = 0; Input.GetKey(KeyCode.Space); i++)
        {
            transform.Translate(0, Mathf.Lerp(floatingRange, -floatingRange, 0.5f * Mathf.Sin(i * Mathf.Deg2Rad) + 0.5f), 0, Space.World);

            if (Input.GetKey(KeyCode.W))
            {
                transform.RotateAround(transform.position, rotAxis, increment);
                //transform.rotation = Quaternion.Slerp(transform.rotation, )
            }
            else if (Input.GetKey(KeyCode.D))
            {
                transform.RotateAround(transform.position, Vector3.up, increment);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                transform.RotateAround(transform.position, Vector3.up, -increment);
            }
            else
            {
                transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));
            }

            yield return new WaitForSeconds(speed);
        }
        */

        exmPos.y = transform.position.y;

        for (int i = 0; i < steps; i++)
        {
            transform.position = new Vector3(
                transform.position.x,
                Mathf.Lerp(ogPos.y, exmPos.y, Mathf.Cos(i * interval * Mathf.Deg2Rad)),
                transform.position.z);

            transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));

            yield return new WaitForSeconds(speed);
        }

        transform.position = ogPos;

        moving = false;
        /*
        moving = true;

        int steps = 15;
        float interval = 90.0f / (float)steps;
        float height = 0.7f;
        float floatingRange = 0.001f;
        float tiltAngle = 70.0f;
        float increment = tiltAngle / steps;

        Vector3 rotAxis;
        { // figures out rotation axis depending on the cameras angle
            Vector3 camPos = mainCam.gameObject.transform.position;
            camPos.y = transform.position.y;

            Vector3 localRotAxis = Vector3.Normalize(camPos - transform.position);

            if (localRotAxis.x > 0 && localRotAxis.z > 0)
                rotAxis = Vector3.left + Vector3.forward;
            else if (localRotAxis.x < 0 && localRotAxis.z > 0)
                rotAxis = Vector3.left + Vector3.back;
            else if (localRotAxis.x < 0 && localRotAxis.z < 0)
                rotAxis = Vector3.right + Vector3.back;
            else
                rotAxis = Vector3.right + Vector3.forward;
        }


        Vector3 ogPos = transform.position;
        Vector3 exmPos = ogPos + new Vector3(0, height, 0);
        Quaternion ogRot = transform.rotation;

        for (int i = 0; i < steps && Input.GetKey(KeyCode.Space); i++)
        {
            transform.position = new Vector3(
                transform.position.x,
                Mathf.Lerp(ogPos.y, exmPos.y + floatingRange, Mathf.Sin(i * interval * Mathf.Deg2Rad)),
                transform.position.z);

            transform.RotateAround(transform.position, rotAxis, increment);

            yield return new WaitForSeconds(speed);
        }

        for (int i = 0; Input.GetKey(KeyCode.Space); i++)
        {
            transform.Translate(0, Mathf.Lerp(floatingRange, -floatingRange, 0.5f * Mathf.Sin(i * Mathf.Deg2Rad) + 0.5f), 0, Space.World);
            yield return new WaitForSeconds(speed);
        }

        exmPos.y = transform.position.y;

        for (int i = 0; i < steps; i++)
        {
            transform.position = new Vector3(
                transform.position.x,
                Mathf.Lerp(ogPos.y, exmPos.y, Mathf.Cos(i * interval * Mathf.Deg2Rad)),
                transform.position.z);

            transform.rotation = Quaternion.Slerp(ogRot, transform.rotation, Mathf.Cos(i * interval * Mathf.Deg2Rad));

            yield return new WaitForSeconds(speed);
        }

        transform.position = ogPos;

        moving = false;
        */
    }
    /* */


    /*
     * State Control functions
     */
     // Call if block is on a light tile
    short UpdateLightTile(Transform tile)
    {
        // if bottom side and tile are both lit, shorts the block
        if (sideState[bottomSide] && tile.GetComponent<LightTileController>().GetState())
        {
            ShortBlock();
            ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.Shorted());
            return -1;
        }
        // if bottom side is lit and tile is not or vice versa
        else if (sideState[bottomSide] != tile.GetComponent<LightTileController>().GetState())
        {
            // changes tile state, updates record of which sides lit and runs a function to update the texture
            tile.GetComponent<LightTileController>().UpdateTile();
            sideState[bottomSide] = !sideState[bottomSide];

            UpdateTextures();

            if (IsWin())
                return 2;
            return 1;
        }

        return 0;
    }

    // Warp Tile
    short Warp(Transform tile, int tilePair)
    {
        AudioSource.PlayClipAtPoint(warpSound, transform.position);
        
        // Get list of all warp tiles
        var objects = GameObject.FindGameObjectsWithTag("WarpTile");

        // Find the other warp tile of that pair
        foreach (var obj in objects)
        {
            WarpData thisPair = obj.GetComponent<WarpData>();

            // If this tile belongs to the correct pair
            if (thisPair.pair == tilePair)
            {
                // Check if this is the current tile
                if (obj.transform == tile.transform)
                {
                    // Do nothing
                    continue;
                }
                
                // If it is not
                else
                {
                    // MOVE TO THIS TILE
                    Vector3 temp = obj.transform.position;
                    temp.y = temp.y + .5f;
                    transform.position = temp;
                    break;
                }
            }

            else
            {
                // Do nothing
                continue;
            }
        }

        return 3;
    }

    // Changes state of block (lit, unlit, shorted), and issues commands to tiles
    short UpdateGameState()
    {
        UpdateBottom();

        // Shoots a ray in the negative y direction and either shorts block, changes the tile state, or does nothing
        RaycastHit[] hits;
        hits = Physics.RaycastAll(this.transform.position, Vector3.down, 2);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag == "LightTile")
            {
                return UpdateLightTile(hits[i].transform);
            }

            // If Warp Tile
            else if (hits[i].transform.tag == "WarpTile")
            {
                WarpData myPair = hits[i].transform.gameObject.GetComponent<WarpData>();
                return Warp(hits[i].transform, myPair.pair);
            }
        }
        return 0;
    }

    // figures out what face is on the -y (world view) side
    void UpdateBottom()
    {
        float bias = 0.25f;
        Vector3 wrldPos = transform.position;
        wrldPos.y -= 0.5f;
        Vector3 loclPos = transform.InverseTransformPoint(wrldPos);

        if (loclPos.x > bias)
            bottomSide = 2;
        else if (loclPos.x < -bias)
            bottomSide = 4;
        else if (loclPos.y > bias)
            bottomSide = 0;
        else if (loclPos.y < -bias)
            bottomSide = 5;
        else if (loclPos.z > bias)
            bottomSide = 1;
        else if (loclPos.z < -bias)
            bottomSide = 3;
    }

    // Changes  teh textures for each side
    void UpdateTextures()
    {
        for (int i = 0; i < 6; i++)
        {
            if (sideState[i])
            {
                transform.GetChild(i).GetComponent<MeshRenderer>().material = lit;
                transform.GetChild(i+6).GetComponent<Light>().intensity = lightIntensity;
            }
            else
            {
                transform.GetChild(i).GetComponent<MeshRenderer>().material = unlit;
                transform.GetChild(i + 6).GetComponent<Light>().intensity = 0.0f;
            }
        }
        isShorted = false;
    }

    // puts the block in the shorted state
    void ShortBlock()
    {
        for (int i = 0; i < 6; i++)
        {
            transform.GetChild(i).GetComponent<MeshRenderer>().material = shorted;
            transform.GetChild(i + 6).GetComponent<Light>().intensity = 0.0f;
        }
        blockStateHUD.GetComponent<BlockUIController>().ShortHUD();

        foreach (GameObject tile in lightTiles)
        {
            //Debug.Log(tile.name);
            tile.GetComponent<LightTileController>().ShortTile();
        }

        isShorted = true;
    }

    void RestartState()
    {
        for (int i = 0; i < 6; i++)
            sideState[i] = false;

        UpdateTextures();
        UpdateBottom();

        // resets all lightTiles to original state
        foreach (GameObject tile in lightTiles)
        {
            //Debug.Log(tile.name);
            tile.GetComponent<LightTileController>().RestartTile();
        }

        ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.AfterRestart());
    }

    void UndoState(short playingState)
    {
        if (playingState == -1)
        { 
            UpdateTextures();
            foreach (GameObject tile in lightTiles)
            {
                //Debug.Log(tile.name);
                tile.GetComponent<LightTileController>().UnshortTile();
            }
        }

        UpdateBottom();

        // Shoots a ray in the negative y direction and either shorts block, changes the tile state, or does nothing
        RaycastHit[] hits;
        hits = Physics.RaycastAll(this.transform.position, Vector3.down, 2);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform.tag == "LightTile")
            {
                // swaps lit block face and blank tile
                if (playingState == 1 /*&& sideState[bottomSide] != hits[i].transform.GetComponent<TileController>().GetState()*/)
                {
                    // changes tile state, updates record of which sides lit and runs a function to update the texture
                    hits[i].transform.GetComponent<LightTileController>().UpdateTile();
                    sideState[bottomSide] = !sideState[bottomSide];

                    UpdateTextures();
                }
            }

            else if (hits[i].transform.tag == "WarpTile")
            {
                WarpData myPair = hits[i].transform.gameObject.GetComponent<WarpData>();
                Warp(hits[i].transform, myPair.pair);
            }
        }
    }

    // returns if a win has occured
    bool IsWin()
    {
        for (int i = 0; i < sideState.Length; i++)
            if (!sideState[i])
                return false;
        return true;
    }
    /* */


    /*
     * BlockControllerMsgTarget functions (AfterMove, AfterUndo, AfterRestart)
     */
    public void AfterMove()
    {
        if (undoing)
        {
            ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.AfterUndo());
            undoing = false;
        }
        else
        {
            short playingState = UpdateGameState();

            if (playingState == 3)
                ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.AfterWarp());

            moveHist.Add((moveDir, playingState));
            if (playingState == 2)
            {
                ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.Win());
            }
        }

        if (!isShorted)
            blockStateHUD.GetComponent<BlockUIController>().UpdateHUD();
        moving = false;
    }

    public void AfterUndo() { }

    public void AfterRestart() 
    {
        blockStateHUD.GetComponent<BlockUIController>().UpdateHUD();
    }

    public void AfterWarp() { }

    public void Shorted() { }

    public void Win()
    {
        levelWon = true;

        GameObject.FindWithTag("PauseMenu").GetComponent<PauseMenu_Levels>().DisableMenu();
        StartCoroutine("Float");
        AudioSource.PlayClipAtPoint(winSound, transform.position);

        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().win = true;

        // Score!
        int moves = moveHist.Count;
        ScoreData thisPar = GameObject.FindObjectOfType<ScoreData>();
        int thisScore = (100 * thisPar.par) / (moves);

        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().UpdateLevelScore(thisScore);
    }

    public void DisableMove()
    {
        moving = true;
    }

    public void EnableMove()
    {
        moving = false;
    }
    /* */

    public void DisableMovement()
    {
        ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.DisableMove());
    }

    public void EnableMovement()
    {
        ExecuteEvents.Execute<BlockControllerMsgTarget>(this.gameObject, null, (x, y) => x.EnableMove());
    }


    public bool[] GetSideState(){ return sideState; }

    // used for the HUD to get information about side state
    // Gets information about where a side is for the HUD
    public bool HUDSideState(int i)
    {
        float bias = 0.25f;
        Vector3 wrldPos = this.transform.position;
        Vector3 loclPos = new Vector3(0.0f, 0.0f, 0.0f);

        int side = -1;

        switch (i)
        {
            // top
            case 0:
                wrldPos.y += 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;

            // front
            case 1:
                wrldPos.z += 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;

            // right
            case 2:
                wrldPos.x += 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;

            // back
            case 3:
                wrldPos.z -= 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;

            // left
            case 4:
                wrldPos.x -= 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;

            // bottom
            case 5:
                wrldPos.y -= 0.5f;
                loclPos = this.transform.InverseTransformPoint(wrldPos);
                break;
        }

        if (loclPos.x > bias)
            side = 2;
        else if (loclPos.x < -bias)
            side = 4;
        else if (loclPos.y > bias)
            side = 0;
        else if (loclPos.y < -bias)
            side = 5;
        else if (loclPos.z > bias)
            side = 1;
        else if (loclPos.z < -bias)
            side = 3;

        if (side == -1)
            return false;
        return sideState[side];
    }
}
