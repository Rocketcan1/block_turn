﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockUIController : MonoBehaviour
{
    GameObject block;

    bool firstCall = true;

    // Start is called before the first frame update
    void Start()
    {
        block = GameObject.FindWithTag("Block");

        UpdateHUD();
    }

    public void UpdateHUD()
    {
        if (firstCall) 
        {
            block = GameObject.FindWithTag("Block");
            firstCall = false;
        }

        for (int i = 0; i < 6; i++)
        {
            if (block.GetComponent<BlockController>().HUDSideState(i))
            {
                //Debug.Log("Setting to lit color" + ": " + block.GetComponent<BlockStateControl>().GetSideState(1));
                this.gameObject.transform.GetChild(i).GetComponent<Image>().color = new Color(1.0f, 0.9655f, 0.0f, 1.0f);
            }
            else
            {
                //Debug.Log("Setting to unlit color" + ": " + block.GetComponent<BlockStateControl>().GetSideState(1));
                this.gameObject.transform.GetChild(i).GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }
        }
    }

    public void ShortHUD()
    {
        for (int i = 0; i < 6; i++)
        {
            this.gameObject.transform.GetChild(i).GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        }
    }
}
