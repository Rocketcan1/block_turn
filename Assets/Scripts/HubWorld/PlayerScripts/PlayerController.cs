﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float sensitivityX = 500f;
    public float sensitivityY = 500f;

    public float verticalAngle = 60f;
    public float speed = 5f;

    public float rayDist = 2.0f;

    public float xRotation = 0f;

    public float blockSpeed = 0.01f;

    Transform camTransform;

    CharacterController controller;

    // related to dropping a block
    public GameObject unlitBlock;
    public GameObject litBlock;
    bool blockDropping = false;
    bool lockMovement = false;
    Vector3 blockMovePoint = Vector3.zero;

    // skips animations if true
    bool skipAnimation = false;
    public GameObject skippingInstructions;
    float popUpTimeout = 0.5f;
    float doubleTap = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        camTransform = Camera.main.transform;
        controller = GetComponent<CharacterController>();

        unlitBlock.SetActive(false);
        litBlock.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindWithTag("GameStateController").GetComponent<GameState>().collectBlock)
        {
            camTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

            CollectBlock(GameObject.FindWithTag("GameStateController").GetComponent<GameState>().GetLevelSelectorPos());
            GameObject.FindWithTag("GameStateController").GetComponent<GameState>().collectBlock = false;
        }

        if (!lockMovement)
        {
            /* Mouse Camera Rotation
            {
                float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * sensitivityX;
                xRotation -= Input.GetAxis("Mouse Y") * Time.deltaTime * sensitivityY;

                xRotation = Mathf.Clamp(xRotation, -verticalAngle, verticalAngle);

                camTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                transform.Rotate(Vector3.up * mouseX);

            }
            //*/

            // Keyboard Movement
            {
                int zAxis = 0;
                int xAxis = 0;

                if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
                    zAxis = 1;
                else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
                    zAxis = -1;
                if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
                    xAxis = 1;
                else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
                    xAxis = -1;

                Vector3 move = transform.right * xAxis + transform.forward * zAxis;

                float moveSpeed = speed;

                if (Input.GetKey(KeyCode.LeftShift))
                {
                    moveSpeed *= 2;
                }

                controller.Move(move * moveSpeed * Time.deltaTime);
            }

            //* Mouse Camera Rotation
            {
                float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * sensitivityX;
                xRotation -= Input.GetAxis("Mouse Y") * Time.deltaTime * sensitivityY;

                xRotation = Mathf.Clamp(xRotation, -verticalAngle, verticalAngle);

                camTransform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                transform.Rotate(Vector3.up * mouseX);

            }
            //*/

            // checks for if e is pressed
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("E pressed");

                    RaycastHit[] levelHits;
                    levelHits = Physics.RaycastAll(camTransform.position, camTransform.TransformDirection(Vector3.forward), rayDist);

                    for (int i = 0; i < levelHits.Length; i++)
                    {
                        if (levelHits[i].transform.tag == "LevelChange")
                        {
                            Debug.Log(levelHits[i].distance + " " + levelHits[i].transform.name);
                            DropBlock(levelHits[i].transform);
                            levelHits[i].transform.parent.GetComponent<LevelSelectController>().LoadScene();
                        }
                        else if (levelHits[i].transform.tag == "BlockHole")
                        {
                            Debug.Log(levelHits[i].distance + " " + levelHits[i].transform.name);
                            if (GameObject.FindWithTag("GameStateController").GetComponent<GameState>().GetBlock())
                            {
                                levelHits[i].transform.GetComponent<BlockHole>().InsertBlock();
                            }
                        }
                    }


                    /*
                    RaycastHit levelHit;
                    if (Physics.Raycast(camTransform.position, camTransform.TransformDirection(Vector3.forward), out levelHit, rayDist))
                    {

                        if (levelHit.transform.tag == "LevelChange")
                        {
                            //Debug.Log(levelHit.distance + " " + levelHit.transform.name);
                            levelHit.transform.parent.GetComponent<EnterLevel>().LoadScene();
                        }
                        else if (levelHit.transform.tag == "BlockHole")
                        {
                            Debug.Log(levelHit.distance + " " + levelHit.transform.name);
                            if (GameObject.FindWithTag("GameStateController").GetComponent<GameState>().GetBlock() && !levelHit.transform.GetComponent<BlockHole>().hasBlock)
                            {
                                levelHit.transform.GetComponent<BlockHole>().InsertBlock();
                            }
                        }
                    }
                    */
                }
            }
        }

        // used to skip animations
        if (blockDropping){
            
            if (doubleTap > 1.0f)
            {
                skipAnimation = true;
                skippingInstructions.SetActive(false);

                doubleTap = 0.0f;
                popUpTimeout = 0.0f;
            }
            else if (doubleTap > 0.0f)
            {
                doubleTap -= Time.deltaTime;
                if (doubleTap < 0.0f)
                    doubleTap = -0.0001f;
            }

            if (popUpTimeout > 0.0f && skippingInstructions.activeSelf)
            {
                popUpTimeout -= Time.deltaTime;
                if (popUpTimeout < 0.0f)
                {
                    skippingInstructions.SetActive(false);
                }
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                doubleTap += 0.75f;
                popUpTimeout += 0.5f;
                skippingInstructions.SetActive(true);
                //Debug.Log("doubleTap: " + doubleTap);
                //Debug.Log("popUpTimeout: " + popUpTimeout);
            }
        }
    }

    public void DropBlock(Transform levelSelector)
    {
        unlitBlock.SetActive(true);

        lockMovement = true;
        blockDropping = true;

        blockMovePoint = levelSelector.position.x * Vector3.right 
            + unlitBlock.transform.position.y * Vector3.up 
            + levelSelector.position.z * Vector3.forward;

        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().SetLevelSelectorPos(levelSelector.position);

        StartCoroutine(DroppingBlock());
    }

    public void CollectBlock(Vector3 levelSelector)
    {
        litBlock.SetActive(true);

        lockMovement = true;
        blockDropping = true;

        blockMovePoint = litBlock.transform.position;

        litBlock.transform.position = levelSelector.x * Vector3.right
            + 0 * Vector3.up
            + levelSelector.z * Vector3.forward;

        StartCoroutine(CollectingBlock());
    }

    public bool GetBlockDropping() { return blockDropping; }

    IEnumerator DroppingBlock()
    {
        GameObject.FindWithTag("HUD").GetComponent<PauseMenu_Hub>().DisableMenu();

        while (true)
        {
            // moves block to center of levelselector
            Vector3 startingPos = unlitBlock.transform.position;
            int blockMoveTime = 125;
            for (int i = 0; i < blockMoveTime * 1.23; i++)
            {
                unlitBlock.transform.position = Vector3.Slerp(startingPos, blockMovePoint, (float)i / (float)blockMoveTime);

                if (skipAnimation)
                    break;

                yield return new WaitForSeconds(blockSpeed);
            }

            if (skipAnimation)
                break;

            float deltaTime = Time.time;
            float velocity = 0.0f;
            float rotationConstant = 50.0f;
            int blockDropTime = 50;

            // drops block into level selector
            for (int i = 0; i < blockDropTime; i++)
            {
                deltaTime = Time.time - deltaTime;
                velocity += 9.81f * deltaTime;

                unlitBlock.transform.position = unlitBlock.transform.position - velocity * deltaTime * Vector3.up;
                unlitBlock.transform.Rotate(rotationConstant * deltaTime, rotationConstant * deltaTime, rotationConstant * deltaTime, Space.World);

                deltaTime = Time.time;

                if (skipAnimation)
                    break;

                yield return new WaitForSeconds(blockSpeed);
            }
            break;
        }

        blockDropping = false;
        //unlitBlock.SetActive(false);
        skipAnimation = false;
        //GameObject.FindWithTag("HUD").GetComponent<PauseMenu_Hub>().EnableMenu();

    }

    IEnumerator CollectingBlock()
    {
        GameObject.FindWithTag("HUD").GetComponent<PauseMenu_Hub>().DisableMenu();

        while (true)
        {
            int blockDropTime = 125;
            Vector3 floatToPosition = litBlock.transform.position.x * Vector3.right + blockMovePoint.y * Vector3.up + litBlock.transform.position.z * Vector3.forward;
            Vector3 startingPos = litBlock.transform.position;

            // makes block float out of level selector
            for (int i = 0; i < blockDropTime * 1.23; i++)
            {
                litBlock.transform.position = Vector3.Slerp(startingPos, floatToPosition, (float)i / (float)blockDropTime);
                litBlock.transform.Rotate(0.0f, 1.0f, 0.0f, Space.World);

                if (skipAnimation)
                    break;

                yield return new WaitForSeconds(blockSpeed);
            }

            if (skipAnimation)
                break;


            startingPos = litBlock.transform.position;
            int blockMoveTime = 150;

            Debug.Log(blockMovePoint);

            // makes block float into you
            for (int i = 0; i < blockMoveTime; i++)
            {
                litBlock.transform.position = Vector3.Slerp(startingPos, blockMovePoint, (float)i / (float)blockMoveTime);
                litBlock.transform.Rotate(0.0f, 1.0f, 0.0f, Space.World);

                if (skipAnimation)
                    break;

                yield return new WaitForSeconds(blockSpeed);
            }

            break;
        }

        blockDropping = false;
        lockMovement = false;
        litBlock.SetActive(false);
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().SetUpdateScore(true);
        skipAnimation = false;

        GameObject.FindWithTag("HUD").GetComponent<PauseMenu_Hub>().EnableMenu();
    }

    public void DisableMovement()
    {
        lockMovement = true;
    }

    public void EnableMovement()
    {
        lockMovement = false;
    }

}
