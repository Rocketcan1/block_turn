﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreData : MonoBehaviour
{
    public int par;

    public int getPar()
    {
        return par;
    }

    public void setPar(int newPair)
    {
        par = newPair;
    }
}
