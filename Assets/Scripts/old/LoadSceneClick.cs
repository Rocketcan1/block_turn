﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneClick : MonoBehaviour
{
    public void LoadScene(int level)
    {
        Debug.Log("IN load scene click LEVEL!!!");
        SceneManager.LoadSceneAsync(level, LoadSceneMode.Additive);
        Debug.Log(SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(level)));
    }
}
