﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    //public Transform screenCenter;

    public Transform sceneCenter;
    public bool useSceneCenter = false;

    public float angle = 90;
    public float speed = 100.0f;
    public int step = 5;


    Vector3 screenCenter;
    bool input = true;

    // -1: 1CW rotation, 1: 1CCW rotation, 0: no rotations (default)
    short orientation = 0;

    // still reqiures a gameobject for scenecenter, but placement doesnt matter
    void GetCenter()
    {
        float x = transform.position.x;
        float y = transform.position.y;
        float z = transform.position.z;

        float rotX = transform.rotation.eulerAngles.x;
        float rotY = transform.rotation.eulerAngles.y;
        float rotZ = transform.rotation.eulerAngles.z;

        float r = y * Mathf.Tan((90-rotX) * Mathf.Deg2Rad);

        float _x = x + r * Mathf.Sin(rotY * Mathf.Deg2Rad);
        float _y = 0.0f;
        float _z = z + r * Mathf.Cos(rotY * Mathf.Deg2Rad);

        screenCenter = new Vector3(_x, _y, _z);
    }

    void Start()
    {
        if (!useSceneCenter)
        {
            GetCenter();
            sceneCenter.position = screenCenter;
        }
        else
        {
            screenCenter = sceneCenter.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (input && Input.GetKeyDown(KeyCode.E) /*&& orientation < 1*/)
        {
            StartCoroutine("RotateCCW");
            input = false;
            orientation++;
        }
        else if (input && Input.GetKeyDown(KeyCode.Q) /*&& orientation > -1*/)
        {
            StartCoroutine("RotateCW");
            input = false;
            orientation--;
        }
    } 

    IEnumerator RotateCW()
    {
        for (int i = 0; i < (angle / step); i++)
        {
            transform.RotateAround(screenCenter, Vector3.up, step);
            yield return new WaitForSeconds(0.01f);
        }

        input = true;
    }

    IEnumerator RotateCCW()
    {
        for (int i = 0; i < (angle / step); i++)
        {
            transform.RotateAround(screenCenter, Vector3.up, -step);
            yield return new WaitForSeconds(0.01f);
        }

        input = true;
    }

    /*
    public void CameraAngle(ref float angleB, ref float angleR)
    {
        Vector3 localCam = sceneCenter.InverseTransformPoint(transform.position + Vector3.down * transform.position.y);

        angleB = Vector3.Angle(localCam, sceneCenter.InverseTransformPoint(screenCenter + Vector3.back));
        angleR = Vector3.Angle(localCam, sceneCenter.InverseTransformPoint(screenCenter + Vector3.right));
    }
    */
}
