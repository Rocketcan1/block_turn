﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public float speed = 5;

    public float rayDist = 1.0f;

    Transform camTransform;

    float xAxis = 0;
    float zAxis = 0;

    CharacterController controller;

    bool movementDisabled = false;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        camTransform = Camera.main.transform;
    }

    void Update()
    {
        if (!movementDisabled)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                //Debug.Log("E pressed");

                RaycastHit levelHit;
                if (Physics.Raycast(camTransform.position, camTransform.TransformDirection(Vector3.forward), out levelHit, rayDist))
                {

                    if (levelHit.transform.tag == "LevelChange")
                    {
                        //Debug.Log(levelHit.distance + " " + levelHit.transform.name);
                        levelHit.transform.parent.GetComponent<EnterLevel>().LoadScene();
                    }
                }
            }

            xAxis = Input.GetAxis("Horizontal");
            zAxis = Input.GetAxis("Vertical");

            Vector3 move = transform.right * xAxis + transform.forward * zAxis;

            controller.Move(move * speed * Time.deltaTime);
        }
    }

    public void DisableMovement()
    {
        movementDisabled = true;
    }

    public void EnableMovement()
    {
        movementDisabled = true;
    }
}
