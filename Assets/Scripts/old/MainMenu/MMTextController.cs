﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MMTextController : MonoBehaviour
{
    public Color selected = new Color(1.0f, 1.0f, 0.5647f, 1.0f);
    public Color unselected = new Color(189.6f / 255.0f, 222.9f / 255.0f, 232.6f / 255.0f, 1.0f);

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.color = unselected;
    }

    void OnMouseEnter() 
    {
        Debug.Log("over");
        GetComponent<Renderer>().material.color = selected;
    }

    public void LoadScene(int level)
    {
        SceneManager.LoadScene(level);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
