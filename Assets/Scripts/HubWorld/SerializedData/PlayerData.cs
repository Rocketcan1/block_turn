﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public float[] position;
    public float[] rotation;
    //public float[] camPosition;
    //public float[] camRotation;

    public PlayerData(Player player)
    {
        position = new float[3];
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;

        rotation = new float[2];

        //rotation[0] = GameObject.FindWithTag("Player").transform.GetChild(0).GetComponent<SmoothMouseLook>().rotationX;
        //rotation[1] = GameObject.FindWithTag("Player").transform.GetChild(0).GetComponent<SmoothMouseLook>().rotationY;

        rotation[0] = GameObject.FindWithTag("Player").transform.GetComponent<PlayerController>().xRotation;
        rotation[1] = GameObject.FindWithTag("Player").transform.eulerAngles[1];
    }
}