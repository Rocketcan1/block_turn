﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu_Hub : MonoBehaviour
{
    public GameObject pauseMenu;

    bool menuOpen = false;
    bool menuDisabled = false;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!menuOpen && Input.GetKeyDown(KeyCode.Escape) && !menuDisabled)
        {
            OpenMenu();
        }
        else if (menuOpen && Input.GetKeyDown(KeyCode.Escape) && !menuDisabled)
        {
            CloseMenu();
        }
    }

    public void DisableMenu()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenu.SetActive(false);
        menuOpen = false;
        menuDisabled = true;
    }

    public void EnableMenu()
    {
        menuDisabled = false;
    }

    public void OpenMenu()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        GameObject.FindWithTag("Player").GetComponent<Player>().DisableMovement();
        pauseMenu.SetActive(true);
        menuOpen = true;
    }

    public void CloseMenu()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.FindWithTag("Player").GetComponent<Player>().EnableMovement();
        pauseMenu.SetActive(false);
        menuOpen = false;
    }

    public void SaveGame()
    {
        GameObject.FindWithTag("GameStateController").GetComponent<GameState>().SaveState();
        Debug.Log("Game Saved");
    }

    public void LoadMenu()
    {
        SaveGame();
        StartCoroutine(LoadAsyncScene());
    }

    IEnumerator LoadAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void QuitGame()
    {
        SaveGame();
        Application.Quit();
    }
}
