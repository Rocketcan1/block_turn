﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallOpen : MonoBehaviour
{
    public int wallIndex;

    public AudioClip openSound;

    public enum Direction { Left, Right }
    public Direction direction;

    public bool wallOpen = false;
    public float dist = 10f;
    public float waitTime = .01f;
    public float speed = 100f;

    GameObject controller;

    bool opening = false;

    // Start is called before the first frame update
    void Start()
    {
         controller = GameObject.FindWithTag("GameStateController");
    }

    // Update is called once per farame
    void Update()
    {
        //Debug.Log(controller.GetComponent<GameStateController>().firstLevel);

        if (!wallOpen && controller.GetComponent<GameState>().openedWalls >= wallIndex && !controller.GetComponent<GameState>().openingWall)
        {
            wallOpen = true;

            if (direction == Direction.Left)
                transform.Translate(Vector3.left * dist);
            else
                transform.Translate(Vector3.right * dist);
        }
        else if (!opening && !wallOpen && controller.GetComponent<GameState>().openedWalls >= wallIndex && controller.GetComponent<GameState>().openingWall)
        {
            wallOpen = true;
            StartCoroutine(OpenWall());
            controller.GetComponent<GameState>().SaveState();
        }
    }

    IEnumerator OpenWall()
    {
        //Debug.Log("Opening Wall");
        opening = true;
        AudioSource.PlayClipAtPoint(openSound, transform.position);

        yield return new WaitForSecondsRealtime(2);

        for (int i = 0; i < dist * (speed); i++)
        {
            if (direction == Direction.Left)
                transform.Translate(Vector3.left / speed);
            else
                transform.Translate(Vector3.right / speed);

            //Debug.Log(i + " | " + (-transform.right) / speed);
            //Debug.Log(i + " | " + dist * speed);

            yield return new WaitForSeconds(waitTime);
        }

        controller.GetComponent<GameState>().openingWall = false;
        opening = false;
    }
}
